SET check_function_bodies = false;
CREATE SCHEMA websliver;
CREATE TABLE websliver.entry (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    title text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    workspace_id uuid NOT NULL,
    location text DEFAULT ''::text NOT NULL,
    tags jsonb DEFAULT jsonb_build_array() NOT NULL,
    favicon_id uuid
);
COMMENT ON TABLE websliver.entry IS 'An entry represents a saved location meta object.';
CREATE FUNCTION websliver.replace_tags(old_tags jsonb, new_tags jsonb, workspace uuid) RETURNS SETOF websliver.entry
    LANGUAGE plpgsql
    AS $$
declare
    _oldt text[];
    _newt text[];
begin
    _oldt := array(select distinct jsonb_array_elements_text(old_tags));
    _newt := array(select distinct jsonb_array_elements_text(new_tags));
    return query update websliver.entry
        set tags = tags - (_oldt || _newt) || array_to_json(_newt)::jsonb
        where tags ?| _oldt and workspace_id = workspace
        returning *;
end;
$$;
CREATE TABLE websliver.tag (
    name text NOT NULL,
    workspace_id uuid NOT NULL,
    uses bigint DEFAULT 0 NOT NULL
);
COMMENT ON TABLE websliver.tag IS 'Only used as a return type for replace_tags and search_tags';
CREATE FUNCTION websliver.search_tags() RETURNS SETOF websliver.tag
    LANGUAGE sql STABLE
    AS $$
    select distinct tag as name, websliver.entry.workspace_id as workspace_id, count(websliver.entry.uid) as uses
    from
        websliver.entry,
        jsonb_array_elements_text(tags) as tag
    group by tag, workspace_id
$$;
CREATE FUNCTION websliver.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$;
CREATE FUNCTION websliver.upsert_entries(hasura_session json, entries jsonb, workspace_id uuid) RETURNS SETOF websliver.entry
    LANGUAGE plpgsql
    AS $$
begin
	perform * from websliver.workspace where user_id = (hasura_session->>'x-hasura-user-id') and uid = workspace_id;
	if not found then
		return query select * from websliver.entry limit 0;
	else
	-- note: these need to be kept in sync with any table alterations
		return query
			insert into websliver.entry
			(
				uid,
				title,
				description,
				created_at,
				updated_at,
				workspace_id,
				location,
				tags,
				favicon_id
			)
			select
				case when (elem->>'uid') isnull then gen_random_uuid() else (elem->>'uid')::uuid end,
				case when (elem->>'title') isnull then '' else (elem->>'title')::text end,
				case when (elem->>'description') isnull then '' else (elem->>'description')::text end,
				case when (elem->> 'created_at') isnull then now() else (elem->> 'created_at')::timestamp with time zone end,
				-- updated_at
				now(),
				workspace_id,
				case when (elem->>'location') isnull then '' else (elem->>'location')::text end,
				case when (elem->'tags') isnull then '[]'::jsonb else (elem->'tags') end,
				(elem->>'favicon_id')::uuid
			from jsonb_array_elements(entries) as arr(elem)
			on conflict on constraint entry_title_location_workspace_id_key
			do update set
				-- title, location, workspace_id would be the same
				-- replace value of description, favicon_id with new values only if they're not empty/null
				description =
					case when EXCLUDED.description is not null and trim(EXCLUDED.description) != ''  then
						trim(EXCLUDED.description)
					else
						trim(websliver.entry.description)
					end,
				favicon_id =
					case when EXCLUDED.favicon_id is not null then
						EXCLUDED.favicon_id
					else
						websliver.entry.favicon_id
					end,
				-- delete duplicate tags and add new tags
				tags = (websliver.entry.tags - array(select distinct jsonb_array_elements_text(EXCLUDED.tags))) || EXCLUDED.tags
			returning *;
		end if;
end;
$$;
CREATE TABLE websliver.upsert_entries_return_type (
    affected_rows bigint NOT NULL
);
CREATE FUNCTION websliver.upsert_entries_aff_rows(hasura_session json, entries jsonb, workspace_id uuid) RETURNS SETOF websliver.upsert_entries_return_type
    LANGUAGE plpgsql
    AS $$
begin
	return query select count(*) as affected_rows from websliver.upsert_entries(hasura_session, entries, workspace_id);
end;
$$;
CREATE TABLE websliver.device (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    user_id text NOT NULL
);
COMMENT ON TABLE websliver.device IS 'A device is tied to an extension and is a synthetic grouping for visits';
CREATE TABLE websliver.favicon (
    hash text DEFAULT ''::text NOT NULL,
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at time with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    data text DEFAULT ''::text NOT NULL,
    user_id text NOT NULL
);
COMMENT ON TABLE websliver.favicon IS 'The favicon for a location. Reason for hash is that some favicon data is longer than what PG allows for unique constraints';
CREATE TABLE websliver."user" (
    uid text DEFAULT (public.gen_random_uuid())::text NOT NULL,
    username text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    last_login_at timestamp with time zone,
    name text,
    email text NOT NULL
);
COMMENT ON TABLE websliver."user" IS 'The owner of one or more devices and one or more workspaces';
COMMENT ON COLUMN websliver."user".username IS 'managed by auth';
COMMENT ON COLUMN websliver."user".email IS 'managed by auth';
CREATE TABLE websliver.visit (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    source_id uuid,
    device_id uuid,
    location text NOT NULL,
    title text
);
COMMENT ON TABLE websliver.visit IS 'A historical visit of a location. Should be create only. source_id represents the parent visit if any.';
CREATE TABLE websliver.workspace (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    name text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    "default" boolean DEFAULT false NOT NULL,
    user_id text NOT NULL
);
COMMENT ON TABLE websliver.workspace IS 'A workspace represents an isolated group of tags and bookmarks';
ALTER TABLE ONLY websliver.entry
    ADD CONSTRAINT "Entry_pkey" PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.device
    ADD CONSTRAINT device_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.entry
    ADD CONSTRAINT entry_title_location_workspace_id_key UNIQUE (title, location, workspace_id);
ALTER TABLE ONLY websliver.favicon
    ADD CONSTRAINT favicon_hash_user_id_key UNIQUE (hash, user_id);
ALTER TABLE ONLY websliver.favicon
    ADD CONSTRAINT favicon_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (name);
ALTER TABLE ONLY websliver.upsert_entries_return_type
    ADD CONSTRAINT upsert_entries_return_type_pkey PRIMARY KEY (affected_rows);
ALTER TABLE ONLY websliver."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.workspace
    ADD CONSTRAINT workspace_pkey PRIMARY KEY (uid);
CREATE TRIGGER set_websliver_device_updated_at BEFORE UPDATE ON websliver.device FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_device_updated_at ON websliver.device IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_entry_updated_at BEFORE UPDATE ON websliver.entry FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_entry_updated_at ON websliver.entry IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_favicon_updated_at BEFORE UPDATE ON websliver.favicon FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_favicon_updated_at ON websliver.favicon IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_user_updated_at BEFORE UPDATE ON websliver."user" FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_user_updated_at ON websliver."user" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_workspace_updated_at BEFORE UPDATE ON websliver.workspace FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_workspace_updated_at ON websliver.workspace IS 'trigger to set value of column "updated_at" to current timestamp on row update';
ALTER TABLE ONLY websliver.device
    ADD CONSTRAINT device_user_id_fkey FOREIGN KEY (user_id) REFERENCES websliver."user"(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.entry
    ADD CONSTRAINT entry_favicon_id_fkey FOREIGN KEY (favicon_id) REFERENCES websliver.favicon(uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY websliver.entry
    ADD CONSTRAINT entry_workspace_id_fkey FOREIGN KEY (workspace_id) REFERENCES websliver.workspace(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.favicon
    ADD CONSTRAINT favicon_user_id_fkey FOREIGN KEY (user_id) REFERENCES websliver."user"(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.tag
    ADD CONSTRAINT tag_workspace_id_fkey FOREIGN KEY (workspace_id) REFERENCES websliver.workspace(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_device_id_fkey FOREIGN KEY (device_id) REFERENCES websliver.device(uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_source_id_fkey FOREIGN KEY (source_id) REFERENCES websliver.visit(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.workspace
    ADD CONSTRAINT workspace_user_id_fkey FOREIGN KEY (user_id) REFERENCES websliver."user"(uid) ON UPDATE CASCADE ON DELETE CASCADE;
