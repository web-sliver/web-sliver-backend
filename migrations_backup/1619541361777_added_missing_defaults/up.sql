CREATE SCHEMA websliver;
CREATE TABLE websliver.bkm_tag_link (
    bookmark_id uuid NOT NULL,
    tag_id uuid NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);
CREATE TABLE websliver.bookmark (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    title text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    location_id uuid,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    extra jsonb DEFAULT jsonb_build_object()
);
CREATE TABLE websliver.device (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);
CREATE TABLE websliver.favicon (
    hash text DEFAULT ''::text NOT NULL,
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at time without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    data text DEFAULT ''::text NOT NULL
);
COMMENT ON TABLE websliver.favicon IS 'Reason for hash is that some favicon data is longer than what PG allows for unique constraints';
CREATE TABLE websliver.location (
    url text DEFAULT ''::text NOT NULL,
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    favicon_id uuid,
    title text DEFAULT ''::text NOT NULL
);
CREATE TABLE websliver.tag (
    title text NOT NULL,
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);
CREATE TABLE websliver.visit (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    location_id uuid NOT NULL
);
CREATE TABLE websliver."window" (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    external_id text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    device_id uuid NOT NULL,
    title text DEFAULT ''::text NOT NULL
);
CREATE TABLE websliver.window_tab (
    window_id uuid NOT NULL,
    location_id uuid NOT NULL,
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "position" integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    active boolean DEFAULT false NOT NULL,
    discarded boolean DEFAULT false NOT NULL,
    external_id text,
    incognito boolean DEFAULT false NOT NULL,
    last_accessed timestamp without time zone,
    pinned boolean DEFAULT false NOT NULL,
    status text
);
ALTER TABLE ONLY websliver.bkm_tag_link
    ADD CONSTRAINT "BookmarkTagLink_pkey" PRIMARY KEY (bookmark_id, tag_id);
ALTER TABLE ONLY websliver.bookmark
    ADD CONSTRAINT "Bookmark_pkey" PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.location
    ADD CONSTRAINT "Location_pkey" PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.location
    ADD CONSTRAINT "Location_url_key" UNIQUE (url);
ALTER TABLE ONLY websliver.tag
    ADD CONSTRAINT "Tag_pkey" PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.tag
    ADD CONSTRAINT "Tag_title_key" UNIQUE (title);
ALTER TABLE ONLY websliver.bookmark
    ADD CONSTRAINT bookmark_title_location_id_key UNIQUE (title, location_id);
ALTER TABLE ONLY websliver.device
    ADD CONSTRAINT device_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.favicon
    ADD CONSTRAINT favicon_hash_key UNIQUE (hash);
ALTER TABLE ONLY websliver.favicon
    ADD CONSTRAINT favicon_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver."window"
    ADD CONSTRAINT window_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.window_tab
    ADD CONSTRAINT window_tab_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.bkm_tag_link
    ADD CONSTRAINT "BookmarkTagLink_bookmark_id_fkey" FOREIGN KEY (bookmark_id) REFERENCES websliver.bookmark(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.bkm_tag_link
    ADD CONSTRAINT "BookmarkTagLink_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES websliver.tag(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.bookmark
    ADD CONSTRAINT "Bookmark_location_id_fkey" FOREIGN KEY (location_id) REFERENCES websliver.location(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.location
    ADD CONSTRAINT location_favicon_id_fkey FOREIGN KEY (favicon_id) REFERENCES websliver.favicon(uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_location_id_fkey FOREIGN KEY (location_id) REFERENCES websliver.location(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver."window"
    ADD CONSTRAINT window_device_id_fkey FOREIGN KEY (device_id) REFERENCES websliver.device(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.window_tab
    ADD CONSTRAINT window_tab_location_id_fkey FOREIGN KEY (location_id) REFERENCES websliver.location(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.window_tab
    ADD CONSTRAINT window_tab_window_id_fkey FOREIGN KEY (window_id) REFERENCES websliver."window"(uid) ON UPDATE CASCADE ON DELETE CASCADE;
