SET check_function_bodies = false;
CREATE SCHEMA websliver;
CREATE FUNCTION websliver.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$;
CREATE TABLE websliver.device (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    user_id uuid NOT NULL
);
COMMENT ON TABLE websliver.device IS 'A device is tied to an extension and is a synthetic grouping for visits';
CREATE TABLE websliver.entry (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    title text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    location_id uuid,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    extra jsonb DEFAULT jsonb_build_object(),
    workspace_id uuid NOT NULL
);
COMMENT ON TABLE websliver.entry IS 'An entry represents a saved location meta object.';
CREATE TABLE websliver.entry_tag (
    entry_id uuid NOT NULL,
    tag_id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE websliver.entry_tag IS 'Many to Many link table for Entries and Tags';
CREATE TABLE websliver.favicon (
    hash text DEFAULT ''::text NOT NULL,
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at time with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    data text DEFAULT ''::text NOT NULL
);
COMMENT ON TABLE websliver.favicon IS 'The favicon for a location. Reason for hash is that some favicon data is longer than what PG allows for unique constraints';
CREATE TABLE websliver.location (
    url text DEFAULT ''::text NOT NULL,
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    favicon_id uuid,
    title text DEFAULT ''::text NOT NULL
);
COMMENT ON TABLE websliver.location IS 'A network location/URL';
CREATE TABLE websliver.tag (
    title text NOT NULL,
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    workspace_id uuid NOT NULL
);
COMMENT ON TABLE websliver.tag IS 'A tag/label for a one or more entries';
CREATE TABLE websliver."user" (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    username text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    last_login_at timestamp with time zone
);
COMMENT ON TABLE websliver."user" IS 'The owner of one or more devices and one or more workspaces';
CREATE TABLE websliver.visit (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    location_id uuid NOT NULL,
    source_id uuid,
    device_id uuid
);
COMMENT ON TABLE websliver.visit IS 'A historical visit of a location. Should be create only. source_id represents the parent visit if any.';
CREATE TABLE websliver.workspace (
    uid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    name text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    "default" boolean DEFAULT false NOT NULL,
    user_id uuid NOT NULL
);
COMMENT ON TABLE websliver.workspace IS 'A workspace represents an isolated group of tags and bookmarks';
ALTER TABLE ONLY websliver.entry_tag
    ADD CONSTRAINT "EntryTag_pkey" PRIMARY KEY (entry_id, tag_id);
ALTER TABLE ONLY websliver.entry
    ADD CONSTRAINT "Entry_pkey" PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.location
    ADD CONSTRAINT "Location_pkey" PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.location
    ADD CONSTRAINT "Location_url_key" UNIQUE (url);
ALTER TABLE ONLY websliver.tag
    ADD CONSTRAINT "Tag_pkey" PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.tag
    ADD CONSTRAINT "Tag_title_key" UNIQUE (title);
ALTER TABLE ONLY websliver.device
    ADD CONSTRAINT device_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.entry
    ADD CONSTRAINT entry_title_location_id_workspace_id_key UNIQUE (title, location_id, workspace_id);
ALTER TABLE ONLY websliver.favicon
    ADD CONSTRAINT favicon_hash_key UNIQUE (hash);
ALTER TABLE ONLY websliver.favicon
    ADD CONSTRAINT favicon_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver."user"
    ADD CONSTRAINT user_username_key UNIQUE (username);
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_pkey PRIMARY KEY (uid);
ALTER TABLE ONLY websliver.workspace
    ADD CONSTRAINT workspace_pkey PRIMARY KEY (uid);
CREATE TRIGGER set_websliver_device_updated_at BEFORE UPDATE ON websliver.device FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_device_updated_at ON websliver.device IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_entry_updated_at BEFORE UPDATE ON websliver.entry FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_entry_updated_at ON websliver.entry IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_favicon_updated_at BEFORE UPDATE ON websliver.favicon FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_favicon_updated_at ON websliver.favicon IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_location_updated_at BEFORE UPDATE ON websliver.location FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_location_updated_at ON websliver.location IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_tag_updated_at BEFORE UPDATE ON websliver.tag FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_tag_updated_at ON websliver.tag IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_user_updated_at BEFORE UPDATE ON websliver."user" FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_user_updated_at ON websliver."user" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_websliver_workspace_updated_at BEFORE UPDATE ON websliver.workspace FOR EACH ROW EXECUTE FUNCTION websliver.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_websliver_workspace_updated_at ON websliver.workspace IS 'trigger to set value of column "updated_at" to current timestamp on row update';
ALTER TABLE ONLY websliver.entry
    ADD CONSTRAINT "Entry_location_id_fkey" FOREIGN KEY (location_id) REFERENCES websliver.location(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.device
    ADD CONSTRAINT device_user_id_fkey FOREIGN KEY (user_id) REFERENCES websliver."user"(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.entry_tag
    ADD CONSTRAINT entry_tag_entry_id_fkey FOREIGN KEY (entry_id) REFERENCES websliver.entry(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.entry_tag
    ADD CONSTRAINT entry_tag_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES websliver.tag(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.entry
    ADD CONSTRAINT entry_workspace_id_fkey FOREIGN KEY (workspace_id) REFERENCES websliver.workspace(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.location
    ADD CONSTRAINT location_favicon_id_fkey FOREIGN KEY (favicon_id) REFERENCES websliver.favicon(uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY websliver.tag
    ADD CONSTRAINT tag_workspace_id_fkey FOREIGN KEY (workspace_id) REFERENCES websliver.workspace(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_device_id_fkey FOREIGN KEY (device_id) REFERENCES websliver.device(uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_location_id_fkey FOREIGN KEY (location_id) REFERENCES websliver.location(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.visit
    ADD CONSTRAINT visit_source_id_fkey FOREIGN KEY (source_id) REFERENCES websliver.visit(uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY websliver.workspace
    ADD CONSTRAINT workspace_user_id_fkey FOREIGN KEY (user_id) REFERENCES websliver."user"(uid) ON UPDATE CASCADE ON DELETE CASCADE;
