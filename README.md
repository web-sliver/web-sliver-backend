# WebSliver Hasura Server

## Installation

- Install the hasura cli tool and upgrade it if needed
```
curl -L https://github.com/hasura/graphql-engine/raw/stable/cli/get.sh | zsh
```

## Creating migrations
1. Modify database schema and create migrations through the CLI
2. Create graphql schema using:
```
npx apollo schema:download \
                --header="X-Hasura-Admin-Secret:$HASURA_GRAPHQL_ADMIN_SECRET" \
                --endpoint=$HASURA_GRAPHQL_ENDPOINT/v1/graphql \
                ./websliver/schema.json
```
3. Commit and push

## Migrating an environment

1. Apply migrations using
```
hasura --project websliver migrate apply --database-name default
```
2. Apply metadata using
```
hasura --project websliver metadata apply
```