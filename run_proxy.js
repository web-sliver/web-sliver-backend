// from: https://github.com/localtunnel/localtunnel/issues/248#issuecomment-994892245
const localtunnel = require("localtunnel");

(async () => {
  const tunnel = await localtunnel({ port: process.env.LT_PORT, subdomain: process.env.LT_DOMAIN });

  console.log(tunnel.url);

  tunnel.on("close", () => {
    console.log("\nTunnel closed");
  });

  process.on("SIGINT", () => {
    tunnel.close();
    process.exit(0);
  });
})();