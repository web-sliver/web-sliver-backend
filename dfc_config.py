_server_host = 'http://127.0.0.1:8080'

#? db migration backup order - manual full db schema dump:
#?      db_make_migration
#?      store_migration_version <version>
#?      dea
#?      db_migrate_no_exec
#?      db_migrate_status
#?      metadata_dump
#?      schema_dump

commands = dict(
    console=dict(
        description='start hasura console',
        command="""
            hasura console --no-browser --project websliver --address 127.0.0.1 --console-port 8001 --api-port 8002
        """
    ),
    update_hasura=dict(
        description='update hasura CLI',
        command="""
            curl -L https://github.com/hasura/graphql-engine/raw/stable/cli/get.sh | zsh
        """
    ),
    db_logs=dict(
        description='show logs for database',
        command="""
            docker-compose logs -f --tail 100 websliver-postgres
        """,
    ),
    dbui=dict(
        description='pgcli for the database',
        command="""
            pgcli -h 127.0.0.1 -p 5400 -U postgres
        """
    ),
    # db_make_migration=dict(
    #     command=f"""
    #         {_hasura} migrate create $1 \
    #             --schema websliver \
    #             --database-name default
    #         echo 'INFO: Remember to store migration version using: store_migration_version <migration_version>'
    #     """
    # ),
    # store_migration_version=dict(
    #     command=f"""
    #         echo $1 > {_migration_version_file}
    #         echo 'INFO: Remember to run dea or dc build now. After that run db_migrate_no_exec'
    #     """
    # ),
    db_migrate_no_exec=dict(
        command=f"""
            hasura --project websliver migrate apply --skip-execution --database-name default
        """
    ),
    db_migrate=dict(
        command=f"""
            hasura --project websliver migrate apply --database-name default
        """
    ),
    db_rollback_one=dict(
        command=f"""
            hasura --project websliver migrate apply --database-name default --down 1
        """
    ),

    db_migrate_status=dict(command=f"hasura --project websliver migrate status --database-name default"),

    # possible options for metadata import/export: --endpoint "$server_host"
    # this is redundant since this is already specified in the config.yml
    metadata_dump=dict(command=f"hasura --project websliver metadata export"),
    metadata_reload=dict(command=f"hasura --project websliver metadata apply"),

    schema_dump=dict(
        command=f"""
            npx apollo schema:download \
                --header="X-Hasura-Admin-Secret:$HASURA_GRAPHQL_ADMIN_SECRET" \
                --endpoint=$HASURA_GRAPHQL_ENDPOINT/v1/graphql \
                ./websliver/schema.json
        """
    ),
    start_proxy=dict(
        command="""yarn run start_proxy"""
    )
)
