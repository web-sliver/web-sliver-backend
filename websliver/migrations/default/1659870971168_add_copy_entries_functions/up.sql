drop function if exists websliver.copy_entries;
drop function if exists websliver.move_entries;

create or replace function websliver.copy_entries(
	search_term text,
	search_tags jsonb,
	search_exclusions jsonb,
	hasura_session json,
	source_workspace_id uuid,
	destination_workspace_id uuid
)
returns setof websliver.entry
as $$
declare
	ws_count_source int;
	ws_count_dest int;
begin
	select count(*) from websliver.workspace
		where user_id = (hasura_session->>'x-hasura-user-id') and uid = source_workspace_id
		into ws_count_source;

	select count(*) from websliver.workspace
		where user_id = (hasura_session->>'x-hasura-user-id') and uid = destination_workspace_id
		into ws_count_dest;

	if ws_count_source = 0 or ws_count_dest = 0 then
		return query select * from websliver.entry limit 0;
	else
		-- note: these need to be kept in sync with any table alterations
		return query
			insert into websliver.entry
			(
				uid,
				title,
				description,
				created_at,
				updated_at,
				workspace_id,
				location,
				tags,
				favicon_id
			)
				select
					gen_random_uuid() as uid,
					title,
					description,
					created_at,
					now() as updated_at,
					destination_workspace_id as workspace_id,
					location,
					tags,
					favicon_id

				from websliver.entry
				where
					workspace_id = source_workspace_id
					and (title ~* search_term or location ~* search_term or description ~* search_term)
					and tags ?& array(select distinct jsonb_array_elements_text(search_tags))
					and not (tags ?| array(select distinct jsonb_array_elements_text(search_exclusions)))
			on conflict on constraint entry_title_location_workspace_id_key
			do update set
				-- title, location, workspace_id would be the same
				-- replace value of description, favicon_id with new values only if they're not empty/null
				description =
					case when EXCLUDED.description is not null and trim(EXCLUDED.description) != ''  then
						trim(EXCLUDED.description)
					else
						trim(websliver.entry.description)
					end,
				favicon_id =
					case when EXCLUDED.favicon_id is not null then
						EXCLUDED.favicon_id
					else
						websliver.entry.favicon_id
					end,
				-- delete duplicate tags and add new tags
				tags = (websliver.entry.tags - array(select distinct jsonb_array_elements_text(EXCLUDED.tags))) || EXCLUDED.tags
			returning *;
	end if;

end;
$$ language plpgsql;


create or replace function websliver.copy_entries_aff_rows(
	search_term text,
	search_tags jsonb,
	search_exclusions jsonb,
	hasura_session json,
	source_workspace_id uuid,
	destination_workspace_id uuid
)
returns setof websliver.return_type_affected_rows
as $$
begin
	return query select count(*) as affected_rows from websliver.copy_entries(
		search_term,
		search_tags,
		search_exclusions,
		hasura_session,
		source_workspace_id,
		destination_workspace_id
	);
end;
$$ language plpgsql;
